=== Config Manager

Executing tests with complex configurations can be quite a challenge.
For example, you need to run tests against/on multiple browsers, mobile devices, different languages, etc.
Such a task requires a lot of effort and sometimes can be confusing.
YAF config manager will help to cope with such confusing cases.

The main objects of the config manager:

- Device - device on which the tests are executed, it can be a web browser, a mobile device, a desktop;
- Configuration - a typed object that represents an additional configuration of the execution, such as locale or user for instance;
- Environment - a set of devices and configurations for single test execution, for example, you need to test something in a browser and on a mobile device, with a specific locale and user;
- Execution - the set of environments where you need to execute the tests and parallel settings of the execution;
- Farm - a farm of devices, cloud provider, ... - provider of devices for tests execution.

IMPORTANT: The configuration manager supports many sources of configurations, in the basic package it reads configuration from JSON files.
These readers will also be used in the examples.

==== How to

===== How to use

To use the configuration manager, you should:

. In the folder with test resources create a directory structure (if another structure is not defined)
+
[source,bash]
----
- execute
  |- config
  |- data
  |- device
  |- env
  |- farm
  |- xxx_config_file.json
----

. At least 2 files should be created
+
NOTE: configuration manager supports both JSON files and https://json5.org/[JSON5]
+
.Device File*
+
File with a random name in the directory `device`
+
[source,json5]
----
{
  iphone8_13: { // device name
    type: "mobile",
    udid: "435sdf2fr23d24t4t2",
    mobileOs: "ios",
    mobileOsVersion: "13",
    params: {
        //some externam params
    },
    capabilities: {
      capability1: "value",
      capability1: "value2"
    }
  }
}
----
+
For specific device syntax see link:{source}/_#add#_[BrowserDevice], MobileDevice, ... #(!!ссылка)#
+
.General configuration File*
+
In this file, you should to fill in some sections (* - required):
+
- name* - configuration name;
- envs* - in this block you should specify the list of environments and determine which devices will belong to the particular environment;
- envsNames - if you want to put the configuration of the environments into separate files for further reuse, specify the names of the environments in this block (the files should be placed in the *env* folder);
- configs - if you want to additionally parameterize your run (not only by devices) and specify, for example, a list of user roles, you should enter them in this section as a key/value;
- configNames - the same logic as with the list of environments: you specify the names of configurations (files must be placed in the *config* folder);
- farmNames - if your configuration implies tests execution on farms, you should specify them in this list; farms are matched to devices by type, i.e. if a farm is configured as a web farm, all web devices will run on this farm.
At the same time, if there are 2 farms for a mobile device, but one of them is purely mobile, and the other is both mobile and web, then purely mobile farm will be used by priority.

+
[source,json5]
----
{
  "name": "smoke",
  "farmsNames": [
    "local",
    "bs"
  ],
  "configNames": [
    "sconfig"
  ],
  "configs": [
    {
      "user": "admin",
      "lang": "ru"
    },
    {
      "user": "admin",
      "lang": "eng"
    }
  ],
  "envsNames": [
    "env1",
    "env2"
  ],
  "envs": {
    "set1": {
      "devices": [
        "dev1",
        "dev2",
        "dev3"
      ]
    },
    "set2": {
      "devices": [
        "dev1",
        "dev2",
        "dev3"
      ]
    }
  }
}
----

.Extra configurations file (if you desire to reuse them)
[source,json5]
----
{
  "ruUser": {
    "user": "user1",
    "locale": "ru"
  },
  "enUser": {
    "user": "user1",
    "locale": "en"
  }
}
----

.Extra environments file (if you desire to reuse them)
[source,json5]
----
{
  "env2": {
    "devices": [
      "ios13"
    ]
  }
}
----

.Farms configuration file
[source,json5]
----
{
  bs: {
    url: "http://",
    user: "xxx",
    key: "yyyy",
    supportedTypes: [
      "web"
    ]
  },
  local: {
    supportedTypes: [
      "web",
      "mobile"
    ]
  }
}
----

NOTE: configuration example - #(!!ссылка на семпл)#

===== How to extend

To add additional configuration (similar to locales), create an implementation of the `ConfigurationResolver` class and in the `applyConfiguration` method implement the necessary usage of the configuration.

For example, as implemented in `LocaleConfigurationResolver`

[source,java]
----
@Service
public class LocaleConfigurationResolver extends ConfigurationResolver {

    @Override
    public String getType() {
        return "locale";
    }

    @Override
    public void applyConfiguration(String value, TestExecutionContext testExecutionContext) {
        try {
            testExecutionContext.addPram(Consts.CTX_LOCALE, new Locale(value));
        } catch (Exception e) {
            log.error("Unable to set locale " + value, e);
        }
    }
}
----

If your resolver requires reading additional data, you need to implement class `ConfigurationDataReader`.

This implementation is used to transform loaded data into strictly typed data.
Due to the fact that the reading of this configuration occurs before the initialization of the Spring context, another mechanism is used to provide dynamic resolvers behavior, and you need to register the created resolver using the mechanism https://docs.oracle.com/javase/tutorial/sound/SPI-intro.html[SPI]

Here is an example of how this mechanism works by example.
(This is how `UserConfigurationResolver` works).

In the configuration files #(! link to the part above)# we use only aliases, for example:

[source,json5]
----
 "user": "admin",
----

However, additional data is needed to build a complete full-featured user object.
This data is stored in the `data\%resolverType%` directory (in case of using json file storage for configuration), it is read by the basic configuration reader and then a transformation method is applied to it from the corresponding data reader, and it transforms Map<String, Object> into an object with typed values.

Thus, the `UserDataReader` may look like this:

[source,java]
----
public class UserDataReader extends JsonConfigurationDataReader<DefaultYafUser> {
    @Override
    public String getType() {
        return "user";
    }

    @Override
    public Map<String, DefaultYafUser> transformData(Map<String, Object> dataMap) {
        return mapper.convertValue(dataMap, new TypeReference<Map<String, DefaultYafUser>>() {
        });
    }
}
----

===== How it works

The upper-level process goes as follows:

. When tests are invoked, the required configuration #(link to section)# is read and an execution configuration `ExecutionConfiguration` is generated.
+
Then, in `ExecutionConfiguration` an array of runs of all environments against all configurations is generated.
For example, we have 2 users (administrator, manager) and 2 browsers (Chrome and FF), as a result of this formation, tests will be executed 4 times:
+
- Chrome Admin
- Chrome Manager
- FF Admin
- FF Manager
+
And this principle can be replicated for any type of configuration.

. The original test suite is modified to fit the configuration (more in link:{source} [TestNG] _#link#_)
. When running a particular test, based on the configuration in step 2, we get an `Environment` object, which contains a list of devices to run, as well as a set of configurations.
Then, we go through the list of configurations, find a corresponding resolver #(!!link)# and apply this configuration to the test execution context.

==== Config reading

The framework allows you to store configurations in various forms, and for this it is necessary to implement the `BaseExecutionReader`.Basically, the framework implements reading from JSON. #(!!ссылка)#


