=== YAF reporting

YAF contains basic reporting functionality.
This implementation is a basic one, and we suggest extending it or using a production one.

==== How to

===== How to use

. Enable the reporting service `yaf.report.enabled=true` in YAF configuration file.
. Configure it if necessary based on available options (`YafReportProperties`):
- specify the path to the report template;
- enable report logging (by default, it is shown in the console);
- change/create a new report template (using the Velocity templating tool) #(!!ссылка)#.

===== How to extend

There are two options to modify the reporter:

. Changing the report template (see example `templates/executionReport.vm`).
. Change the algorithm of the report processing.
You can not just output it to the console, but, for instance, form a pdf document and send it via email.
For this, you need to subscribe to the event `ExecutionReportReadyEvent` and add aditional processing logic.

[source,java]
----
@Service
@Slf4j
public class SendEmailPdfReportService {

    @EventListener
    public void sendPdfReport(ExecutionReportReadyEvent event) {
        PdfDocument pdf = pdfService.generate(event.getReport());
        emailService.sendPdf(getSubEmailList(), pdf);
    }

    ...
}
----

===== How it works

The `ReportService` class listens to events as the tests are running, aggregates them in the `ExecutionReport` object and when the `ExecutionFinishEvent` event occurs, this class completes the `ExecutionReport`, prints it (outputs to the console) using the `PrintReportService` and also generates the `ExecutionReportReadyEvent` event, which can be further processed by custom handlers.
