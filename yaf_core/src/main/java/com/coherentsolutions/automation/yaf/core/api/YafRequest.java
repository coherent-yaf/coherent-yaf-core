package com.coherentsolutions.automation.yaf.core.api;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.api.auth.YafApiUser;
import com.coherentsolutions.automation.yaf.core.api.properties.ApiProperties;

public interface YafRequest<T, R> {

    String GET = "GET";
    String POST = "POST";
    String PUT = "PUT";
    String DELETE = "DELETE";

    T anonymousReq() throws YafApiRequestException;

    T req() throws YafApiRequestException;

    T req(YafApiUser user) throws YafApiRequestException;

    R anonymousReq(String method, String url, Object... body) throws YafApiRequestException;

    R req(String method, String url, Object... body) throws YafApiRequestException;

    R req(YafApiUser user, String method, String url, Object... body) throws YafApiRequestException;

    ApiProperties getProps();

    default R get(String url) throws YafApiRequestException {
        return req(GET, url);
    }

    default R post(String url, Object... body) throws YafApiRequestException {
        return req(POST, url, body);
    }

    default R put(String url, Object... body) throws YafApiRequestException {
        return req(PUT, url, body);
    }

    default R delete(String url) throws YafApiRequestException {
        return req(DELETE, url);
    }

    default R delete(String url, Object... body) throws YafApiRequestException {
        return req(DELETE, url, body);
    }

    default R get(YafApiUser user, String url) throws YafApiRequestException {
        return req(user, GET, url);
    }

    default R post(YafApiUser user, String url, Object... body) throws YafApiRequestException {
        return req(user, POST, url, body);
    }

    default R put(YafApiUser user, String url, Object... body) throws YafApiRequestException {
        return req(user, PUT, url, body);
    }

    default R delete(YafApiUser user, String url) throws YafApiRequestException {
        return req(user, DELETE, url);
    }

    default R aGet(String url) throws YafApiRequestException {
        return anonymousReq(GET, url);
    }

    default R aPost(String url, Object... body) throws YafApiRequestException {
        return anonymousReq(POST, url, body);
    }

    default R aPut(String url, Object... body) throws YafApiRequestException {
        return anonymousReq(PUT, url, body);
    }

    default R aDelete(String url) throws YafApiRequestException {
        return anonymousReq(DELETE, url);
    }

}
