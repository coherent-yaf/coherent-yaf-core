
package com.coherentsolutions.automation.yaf.core.condition;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.BrowserDevice;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.DesktopDevice;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.MobileDevice;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.enums.YafEnum;
import com.coherentsolutions.automation.yaf.core.utils.CapabilitiesUtils;
import org.apache.logging.log4j.util.Strings;
import org.openqa.selenium.Dimension;
import org.springframework.stereotype.Component;

@Component
public class BaseConditionMatcher implements ConditionMatcher {

    @Override
    public int matchScore(Device device, YafCondition condition, Class cls, TestExecutionContext testExecutionContext) {

        int score = getCompareEnumScore(device.getType(), condition.deviceType())
                + matchScreenResolution(device, condition, testExecutionContext);
        if (device instanceof BrowserDevice) {
            BrowserDevice browserDevice = (BrowserDevice) device;
            score += getCompareEnumScore(browserDevice.getBrowser(), condition.browser())
                    + getCompareEnumScore(browserDevice.getOs(), condition.os())
                    + getCompareScore(browserDevice.getBrowserVersion(), condition.browserVersion());
        } else if (device instanceof MobileDevice) {
            MobileDevice mobileDevice = (MobileDevice) device;
            score += getCompareEnumScore(mobileDevice.getMobileOs(), condition.mobileOs())
                    + getCompareScore(mobileDevice.getMobileOsVersion(), condition.mobileOsVersion())
                    + getCompareScore(mobileDevice.isSimulator(), condition.simulator());
        } else if (device instanceof DesktopDevice) {
            DesktopDevice desktopDevice = (DesktopDevice) device;
            score += getCompareEnumScore(desktopDevice.getOs(), condition.os())
                    + getCompareScore(desktopDevice.getOsVersion(), condition.osVersion());
        }
        return score;
    }

    // todo select proper place for that
    private int matchScreenResolution(Device device, YafCondition condition,
            TestExecutionContext testExecutionContext) {
        // TODO !!! Check for mobile /desktop possible way to set resolution
        if (condition.width() == 0 && condition.height() == 0) {
            return 0;
        } else {
            if (device.getCapabilities() == null) {
                return 0;
            } else {
                // todo it is better to refactor this!!!!
                String resolution = device.getCapabilities().get(Consts.CAP_RESOLUTION);// screenResolution?
                // screenwidth?
                // window-size
                Dimension dimension = CapabilitiesUtils.getDimensionFromResolution(resolution);

                int score = 0;

                if (dimension == null) {
                    return -1;
                }

                if (condition.width() != 0) {
                    score += dimension.getWidth() <= condition.width() ? 1 : -1;
                }
                if (condition.height() != 0) {
                    score += dimension.getHeight() <= condition.height() ? 1 : -1;
                }
                return score;
            }
        }
    }

    protected int getCompareEnumScore(YafEnum actual, YafEnum expected) {
        if (actual == null || expected.toString().toLowerCase().equals("other")) {
            return 0;
        }
        return compareBoolean(actual.equals(expected));
    }

    protected int getCompareScore(String actual, String expected) {
        if (actual == expected) {
            return 1;
        } else if (expected.equals(Strings.EMPTY)) {
            return 0;
        } else {
            return -1;
        }
    }

    protected int getCompareScore(int actual, int expected) {
        if (actual == expected) {
            return 1;
        } else {
            return -1;
        }
    }

    protected int getCompareScore(boolean actual, boolean expected) {
        if (actual == expected) {
            return 1;
        } else {
            return -1;
        }
    }

    protected int compareBoolean(boolean actual) {
        if (actual) {
            return 1;
        } else {
            return -1;
        }
    }
}
