package com.coherentsolutions.automation.yaf.core.config.env;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.Environment;
import com.coherentsolutions.automation.yaf.core.config.env.domain.ExecutionConfiguration;
import com.coherentsolutions.automation.yaf.core.config.env.domain.RunParallelSetup;
import com.coherentsolutions.automation.yaf.core.config.env.reader.BaseExecutionReader;
import com.coherentsolutions.automation.yaf.core.config.env.reader.json.JsonFilesConfigurationReader;
import com.coherentsolutions.automation.yaf.core.config.env.serviceprovider.SpService;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.exception.EnvSetupYafException;
import com.coherentsolutions.automation.yaf.core.utils.PropertiesUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Optional;

import static com.coherentsolutions.automation.yaf.core.consts.Consts.ENV_SETTINGS_FILE;

@Slf4j
public final class ExecutionService {

    private static ExecutionService instance;
    BaseExecutionReader configurationReader;
    @Getter
    ExecutionConfiguration configuration;
    @Getter
    Map<String, Map<String, Object>> configurationData;
    // SpService spService = new SpService();

    private ExecutionService() {
        String readerType = PropertiesUtils.getPropertyValue("confReader", Consts.JSON);
        String configurationName = PropertiesUtils.getPropertyValue(ENV_SETTINGS_FILE, null);
        // configurationName = "sample";

        switch (readerType) {
        case Consts.JSON:
        default: {
            configurationReader = new JsonFilesConfigurationReader();
        }
        }
        if (StringUtils.isEmpty(configurationName)) {
            // return default config
            configuration = ExecutionConfiguration.getDefault();
        } else {
            try {
                configuration = configurationReader.getExecutionConfiguration(configurationName);
                // spService.getExecutionReader(readerType).getExecutionConfiguration(configurationName);
                configurationData = configurationReader.getConfigData(new SpService());
            } catch (Exception e) {
                throw new EnvSetupYafException("Configuration error. " + e.getMessage(), e);
            }
        }
    }

    public static ExecutionService getInstance() {
        if (instance == null) {
            synchronized (ExecutionService.class) {
                instance = new ExecutionService();
            }
        }
        return instance;
    }

    public RunParallelSetup getRunParallelSetup() {
        return configuration.getSetup() == null ? new RunParallelSetup() : configuration.getSetup();
    }

    public Environment getEnv(String name, TestExecutionContext testExecutionContext) {
        return configuration.getEnvironments().get(name);
    }

    public Optional<Object> getConfigDataValue(String configType, String configKey) {
        try {
            return Optional.of(configurationData.get(configType).get(configKey));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}
