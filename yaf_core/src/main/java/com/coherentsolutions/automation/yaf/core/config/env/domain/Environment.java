package com.coherentsolutions.automation.yaf.core.config.env.domain;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.BrowserDevice;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.enums.Browser;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import com.coherentsolutions.automation.yaf.core.enums.DriverScope;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.util.Base64Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class Environment implements Cloneable {

    String name;
    List<String> devices;
    Map<String, String> configs;
    @JsonIgnore
    List<Device> deviceList;
    @JsonIgnore
    String id;

    public static Environment getDefaultEnv() {
        Environment env = new Environment();
        env.setName(Consts.DEFAULT);
        BrowserDevice browserDevice = new BrowserDevice();
        browserDevice.setName(Consts.DEFAULT).setScope(DriverScope.EXECUTION);
        browserDevice.setBrowser(Browser.CHROME).setBrowserVersion(Consts.LATEST);
        env.addDevice(browserDevice);
        return env;
    }

    public Device findByName(String name) {
        return findByPredicate(e -> e.getName().equals(name));
    }

    public Device findByType(DeviceType type) {
        return findByPredicate(e -> e.getType().equals(type));
    }

    public Device findByPredicate(Predicate<Device> predicate) {
        return deviceList.stream().filter(predicate).findFirst().orElse(null);
    }

    public void addDevice(Device device) {
        if (deviceList == null) {
            deviceList = new ArrayList<>();
        }
        deviceList.add(device);
    }

    @Override
    public Environment clone() throws CloneNotSupportedException {
        Environment e = (Environment) super.clone();
        // TODO think about deep clone deviceList
        return e;
    }

    @JsonIgnore
    public String getId() {
        if (id == null) {
            id = name + "::" + (configs != null
                    ? Base64Utils.encodeToString(configs.values().stream().collect(Collectors.joining("_")).getBytes())
                    : "");
        }
        return id;
    }
}
