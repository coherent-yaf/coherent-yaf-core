package com.coherentsolutions.automation.yaf.core.config.env.reader;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.Config;
import com.coherentsolutions.automation.yaf.core.config.env.domain.Environment;
import com.coherentsolutions.automation.yaf.core.config.env.domain.Execution;
import com.coherentsolutions.automation.yaf.core.config.env.domain.ExecutionConfiguration;
import com.coherentsolutions.automation.yaf.core.config.env.serviceprovider.SpService;
import com.coherentsolutions.automation.yaf.core.exception.EnvSetupYafException;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public abstract class BaseExecutionReader implements ExecutionReader {

    protected abstract Execution getExecution(String configurationName);

    public ExecutionConfiguration getExecutionConfiguration(String configurationName) {
        Execution execution = getExecution(configurationName);

        ExecutionConfiguration configuration = new ExecutionConfiguration();
        configuration.setName(execution.getName());
        configuration.setSetup(execution.getSetup());
        // configuration.setApplications(execution.getApplications());

        // calculate execution plan
        Map<String, Environment> environments = new HashMap<>();
        if (execution.getConfigs() == null) {
            // we have no additional configurations, just envs
            environments = execution.getEnvs();
        } else {
            for (Config c : execution.getConfigs()) {
                for (Environment e : execution.getEnvs().values()) {
                    try {
                        Environment ee = e.clone();
                        ee.setConfigs(c);
                        environments.put(ee.getId(), ee);
                    } catch (CloneNotSupportedException cloneNotSupportedException) {
                        throw new EnvSetupYafException("Unable to clone environment!", cloneNotSupportedException);
                    }
                }
            }
        }

        configuration.setEnvironments(environments);

        return configuration;
    }

    public abstract Map<String, Map<String, Object>> getConfigData(SpService spService);

}
