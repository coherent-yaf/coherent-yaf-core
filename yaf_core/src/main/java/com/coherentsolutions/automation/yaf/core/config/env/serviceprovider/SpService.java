package com.coherentsolutions.automation.yaf.core.config.env.serviceprovider;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.reader.ConfigurationDataReader;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public class SpService {

    Map<String, ConfigurationDataReader> dataReaders;
    // Map<String, ExecutionReader> executionReaders;

    public SpService() {
        // load config readers
        dataReaders = new HashMap<>();
        ServiceLoader<ConfigurationDataReader> cdLoader = ServiceLoader.load(ConfigurationDataReader.class);
        cdLoader.iterator().forEachRemaining(c -> dataReaders.put(c.getType(), c));
        // // load execution readers
        // executionReaders = new HashMap<>();
        // ServiceLoader<ExecutionReader> eLoader = ServiceLoader.load(ExecutionReader.class);
        // eLoader.iterator().forEachRemaining(e -> executionReaders.put(e.getType(), e));
    }

    public ConfigurationDataReader getConfigurationDataReader(String name) {
        return dataReaders.get(name);
    }

    // public ExecutionReader getExecutionReader(String name) {
    // return executionReaders.get(name);
    // }
}
