
package com.coherentsolutions.automation.yaf.core.config.yaf;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.bean.YafBean;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import com.coherentsolutions.automation.yaf.core.pom.YafComponent;
import com.coherentsolutions.automation.yaf.core.pom.factory.DefaultYafLocatorFactory;
import com.coherentsolutions.automation.yaf.core.pom.factory.YafLocatorFactory;
import com.coherentsolutions.automation.yaf.core.utils.templates.TemplateConfig;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@ComponentScan(basePackages = { "com.coherentsolutions" }, includeFilters = @ComponentScan.Filter(YafComponent.class))
@EnableCaching
@EnableAsync
@EnableAutoConfiguration
@EnableAspectJAutoProxy
@Slf4j
public abstract class YafConfig implements AppConfig {

    protected ApplicationArguments arguments;

    @Autowired
    GenericApplicationContext genericApplicationContext;

    @Value("${spring.messages.basename:messages}") // todo config
    String messagesPath;

    public YafConfig(ApplicationArguments args) {
        log.info("Building general execution config");
        arguments = args;
    }

    //
    // @Bean
    // public static CustomScopeConfigurer testScopeConfigurer() {
    // CustomScopeConfigurer customScopeConfigurer = new CustomScopeConfigurer();
    // customScopeConfigurer.addScope(Consts.SCOPE_THREADLOCAL, new SimpleThreadScope());
    // return customScopeConfigurer;
    // }
    //
    @Bean
    public TaskExecutor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }

    @Bean
    public Faker faker() {
        return new Faker(Locale.getDefault());
    }

    @Bean(name = "supportedClasses")
    public List<Class> supportedClasses() {
        List<Class> classes = new ArrayList<>();
        classes.add(YafBean.class);
        return classes;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.enable(JsonParser.Feature.ALLOW_COMMENTS);
        return objectMapper;
    }

    @Bean
    @ConditionalOnMissingBean(YafLocatorFactory.class)
    public YafLocatorFactory locatorFactory() {
        return new DefaultYafLocatorFactory();
    }

    // // no need, lets use simple cache config
    // @Bean
    // public CacheManager cacheManager() {
    // SimpleCacheManager cacheManager = new SimpleCacheManager();
    // List<Cache> caches = new ArrayList<>();
    // caches.add(new ConcurrentMapCache("yafBeanFields"));
    // caches.add(new ConcurrentMapCache("beanFields"));
    // caches.add(new ConcurrentMapCache("beanNames"));
    // caches.add(new ConcurrentMapCache("classChildren"));
    // caches.add(new ConcurrentMapCache("annotation"));
    // caches.add(new ConcurrentMapCache("annotations"));
    // caches.add(new ConcurrentMapCache("beanWaits"));
    // cacheManager.setCaches(caches);
    // return cacheManager;
    // }

    @Bean
    public ResourceBundleMessageSource messageSource() {

        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames(messagesPath);
        source.setDefaultLocale(Locale.US);
        source.setUseCodeAsDefaultMessage(true);
        source.setFallbackToSystemLocale(true);

        return source;
    }

    @Bean
    public Locale locale() {
        return Locale.getDefault();
    }

    @Bean
    public TemplateConfig dataConfJson() {
        return new TemplateConfig("/", ".json");
    }

    @Bean
    public TemplateConfig dataConfXml() {
        return new TemplateConfig("/", ".xml");
    }

    @Bean
    public TemplateConfig dataConfMustache() {
        return new TemplateConfig("/", ".mustache");
    }

    @Component
    @ConfigurationPropertiesBinding
    public static class DeviceTypeConverter implements Converter<String, DeviceType> {

        @Override
        public DeviceType convert(String from) {
            return DeviceType.valueOfName(from.toLowerCase());

        }
    }

    // interceptors

    // @PostConstruct
    // public void registerAspects() {
    //
    // for (MethodCallInterceptor mci: mciList){
    // AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
    // pointcut.setExpression(mci.getExpression());
    // Advisor a = new DefaultPointcutAdvisor(pointcut, loggingMethodInterceptor(mci));
    // //genericApplicationContext.registerBean("asd"+Math.random(), Advisor.class, ()->a);
    // beanFactory.registerSingleton("service..."+Math.random(), a);
    // }
    //
    //
    // }

    // @Bean
    // @Qualifier("advicorManager")
    // public String advisorManager(){
    //
    // for (MethodCallInterceptor mci: mciList){
    // AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
    // pointcut.setExpression(mci.getExpression());
    // Advisor a = new DefaultPointcutAdvisor(pointcut, loggingMethodInterceptor(mci));
    // genericApplicationContext.registerBean("asd"+Math.random(), Advisor.class, ()->a);
    // }
    // return "dddd";
    // }

    // @Bean
    // public Advisor advisor() {
    // AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
    // pointcut.setExpression(methodCallInterceptor.getExpression());
    // return new DefaultPointcutAdvisor(pointcut, loggingMethodInterceptor());
    // }

    // @Autowired
    // MethodCallInterceptor methodCallInterceptor;
    //
    // @Bean
    // public MethodInterceptor loggingMethodInterceptor() {
    // return invocation -> {
    // Map<String, Object> params = methodCallInterceptor.onStart(invocation.getThis(), invocation.getMethod(),
    // invocation.getArguments());
    // Object result = null;
    // Throwable ex = null;
    // try {
    // return invocation.proceed();
    // } catch (Throwable e) {
    // ex = e;
    // throw e;
    // } finally {
    // methodCallInterceptor.onFinish(invocation.getThis(), invocation.getMethod(),
    // invocation.getArguments(), result, ex, params);
    // }
    // };
    // }
    //
    // @Bean
    // public Advisor advisor() {
    // AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
    // pointcut.setExpression(methodCallInterceptor.getExpression());
    // return new DefaultPointcutAdvisor(pointcut, loggingMethodInterceptor());
    // }

}
