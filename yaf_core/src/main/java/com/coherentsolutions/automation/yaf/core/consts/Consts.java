
package com.coherentsolutions.automation.yaf.core.consts;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

public class Consts {

    public static final String FRAMEWORK_NAME = "yaf";

    public static final String UTF8 = "UTF-8";

    public static final String ENV_SEPARATOR = "--";

    public static final String SCOPE_THREADLOCAL = "threadlocal";

    public static final String ENV_SETTINGS_FILE = "envSettings";
    public static final String ENV_SETTING_PARAM = "envSetup";

    public static final String DEFAULT = "default";
    public static final String TESTENV = "testenv";
    public static final String LATEST = "latest";

    public static final String JSON = "json";
    public static final String JSON5 = "json5";
    public static final String XML = "xml";
    public static final String PNG = "png";
    public static final String TXT = "txt";
    public static final String LOG = "log";
    public static final String HTML = "html";

    public static final String PROPERTIES = "properties";

    public static final String FARM_BROWSERSTACK = "bs";

    public static final String CAP_RESOLUTION = "resolution";

    public static final String UNKNOWN = "unknown";

    public static final String JSON_TYPE = "application/json";
    public static final String TEXT_TYPE = "text/plain";
    public static final String HTML_TYPE = "text/html";
    public static final String PNG_TYPE = "image/png";

    public static final String CTX_USER = "user";
    public static final String CTX_LOCALE = "locale";
    public static final String CTX_APP = "application";

}
