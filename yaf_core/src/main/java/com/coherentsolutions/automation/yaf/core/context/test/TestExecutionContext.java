
package com.coherentsolutions.automation.yaf.core.context.test;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.Environment;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.context.execution.ExecutionContext;
import com.coherentsolutions.automation.yaf.core.drivers.manager.DriverManager;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriversStore;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import com.coherentsolutions.automation.yaf.core.enums.DriverScope;
import com.coherentsolutions.automation.yaf.core.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.core.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.core.processor.test.TestMethodArgProcessor;
import com.coherentsolutions.automation.yaf.core.test.model.TestInfo;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
/**
 * Attention, do not autowire this bean, cause it is thread local!. You can get proper instance from application
 * context.
 */
@Data
@Accessors(chain = true)
// @Component
// @org.springframework.context.annotation.Scope(Consts.SCOPE_THREADLOCAL)
@Slf4j
public abstract class TestExecutionContext implements Serializable, DisposableBean {

    protected String testName;
    protected String suiteName;

    protected TestInfo testInfo;

    protected Long startTime;
    protected Environment env;

    protected Map<String, Object> params;

    @Autowired
    protected ExecutionContext executionContext;

    @Autowired
    protected ApplicationContext applicationContext;

    @Autowired
    protected DriverManager driverManager;

    protected DriversStore threadDrivesStore;

    @Autowired
    List<TestMethodArgProcessor> methodArgProcessors;

    @Getter
    Set<DriverHolder> usedInTestDrivers;

    public TestExecutionContext() {
        log.info("Building test context for thread {}", Thread.currentThread().getName());
        threadDrivesStore = new DriversStore();
        params = new HashMap<>();
        usedInTestDrivers = new HashSet<>(); // todo think about other option to mark DH to context
    }

    protected abstract DriverHolder findDriverHolderInContext(Device device);

    protected abstract void addDriverToContext(Device device, DriverHolder driverHolder);

    protected abstract DriverHolder getDriverHolder(DeviceType deviceType, String... name) throws DriverYafException;

    public DriverHolder getWebDriverHolder(String... name) throws DriverYafException {
        return getDriverHolder(DeviceType.WEB, name);
    }

    public DriverHolder getMobileDriverHolder(String... name) throws DriverYafException {
        return getDriverHolder(DeviceType.MOBILE, name);
    }

    public DriverHolder getDesktopDriverHolder(String... name) throws DriverYafException {
        return getDriverHolder(DeviceType.DESKTOP, name);
    }

    public void buildFromTestStartEvent(TestStartEvent event, Environment env) {
        initFromTestStartEvent(event, env);
        if (testInfo.getTestMethodParams() != null) {
            testInfo.getTestMethodParams().entrySet().forEach(e -> {
                TestMethodArgProcessor processor = methodArgProcessors.stream()
                        .filter(p -> p.canProcess(e.getKey(), e.getValue())).findFirst().orElse(null);
                if (processor != null) {
                    processor.processTestMethodArg(e.getKey(), e.getValue(), this);
                }
            });
        }
    }

    protected abstract void initFromTestStartEvent(TestStartEvent event, Environment env);

    public void addPrams(Map<String, Object> p) {
        params.putAll(p);
    }

    public void addPram(String key, Object val) {
        params.put(key, val);
    }

    public Object getParam(String key) {
        return params.get(key);
    }

    public String getStringParam(String key) {
        return (String) getParam(key);
    }

    public void addStringParams(Map<String, String> stringParams) {
        params.putAll(stringParams.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));
    }

    public DriverHolder findInitedDriverHolderByType(DeviceType deviceType) {
        DriverHolder holder = usedInTestDrivers.stream().filter(d -> d.getDevice().getType().equals(deviceType))
                .findFirst().orElse(null);
        if (holder == null) {
            // lets search in execution context
            holder = executionContext.findInitedDriverHolderByType(suiteName, deviceType);
        }
        return holder;
    }

    // public Stream<Map.Entry<Device, DriverHolder>> initedDriverHolders() {
    // Map<Device, DriverHolder> driverHolderMap = threadDrivesStore.getDriverHolderMap();
    // driverHolderMap.putAll(executionContext.initedDriverHoldersBySuite(suiteName));
    // return driverHolderMap.entrySet().stream();
    // }

    protected void clearBeforeNewTest() {
        startTime = null;
        testInfo = null;
        // remove all drivers from testMethodScope
        threadDrivesStore.clearAll(DriverScope.METHOD);
        usedInTestDrivers.clear();
    }

    @Override
    public void destroy() throws Exception {
        clearContext();
    }

    public void clearContext() {
        log.debug("Clearing context");
        // we need to close all drivers
        threadDrivesStore.clearAll();
        params.clear();
        usedInTestDrivers.clear();
    }
}
