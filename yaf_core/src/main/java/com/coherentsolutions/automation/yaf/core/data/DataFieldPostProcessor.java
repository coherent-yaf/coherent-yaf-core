
package com.coherentsolutions.automation.yaf.core.data;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.bean.field.YafFieldProcessor;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.data.reader.DataReader;
import com.coherentsolutions.automation.yaf.core.exception.BeanInitYafException;
import com.coherentsolutions.automation.yaf.core.utils.YafBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

@Service
@Order(1)
@Slf4j
public class DataFieldPostProcessor implements YafFieldProcessor {

    @Autowired
    YafBeanUtils beanUtils;

    @Override
    public boolean canProcess(Field field, Class fieldType, Object obj, Class objType, List<Annotation> annotations,
            TestExecutionContext testExecutionContext) {
        return beanUtils.getAnnotation(fieldType, YafData.class) != null;
    }

    @Override
    public Object processField(Field field, Class fieldType, Object obj, Class objType, List<Annotation> annotations,
            TestExecutionContext testExecutionContext) throws BeanInitYafException {
        YafData data = beanUtils.getAnnotation(fieldType, YafData.class);
        try {
            DataReader dataReader = beanUtils.getBean(data.reader());
            return dataReader.readData(data, fieldType, testExecutionContext);
        } catch (Exception e) {
            throw new BeanInitYafException(e.getMessage(), e);
        }
    }
}
