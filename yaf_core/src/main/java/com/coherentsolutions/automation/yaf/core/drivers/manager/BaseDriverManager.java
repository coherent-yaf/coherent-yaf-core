
package com.coherentsolutions.automation.yaf.core.drivers.manager;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.core.drivers.properties.GeneralDriverProperties;
import com.coherentsolutions.automation.yaf.core.enums.DriverScope;
import com.coherentsolutions.automation.yaf.core.exception.DriverYafException;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@ConditionalOnSingleCandidate(DriverManager.class)
public class BaseDriverManager implements DriverManager {

    @Autowired(required = false)
    List<DriverResolver> resolvers;
    @Autowired(required = false)
    GeneralDriverProperties driverProperties;
    @Autowired(required = false)
    WebDriverListener webDriverListener;

    @Override
    public DriverHolder getDriver(Device device) throws DriverYafException {
        DriverResolver resolver = resolvers.stream().filter(r -> r.canResolve(device)).findFirst().orElse(null);
        if (resolver != null) {
            DriverHolder holder = resolver.initDriver(device);
            if (holder != null) {
                holder.setScope(getDriversScope(holder));
                if (holder.getDevice().getType() == null) {
                    throw new DriverYafException("Please set proper device type for " + device.getName());
                }
                if (driverProperties.isPublishDriverEvents()) {
                    EventFiringWebDriver efwd = new EventFiringWebDriver(holder.getDriver());
                    efwd.register(webDriverListener);
                    holder.setDriver(efwd);
                }
                return holder;
            }
        }
        throw new DriverYafException("Unable to init proper driver for device " + device.getName());
    }

    protected DriverScope getDriversScope(DriverHolder holder) {
        DriverScope scope = holder.getDevice().getScope();
        if (scope == null) {
            scope = holder.getProperties().getScope();
            if (scope == null) {
                scope = DriverScope.EXECUTION;
            }
        }
        return scope;
    }
}
