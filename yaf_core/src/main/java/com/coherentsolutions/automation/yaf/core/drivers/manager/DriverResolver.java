
package com.coherentsolutions.automation.yaf.core.drivers.manager;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.core.drivers.properties.DriverProperties;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import com.coherentsolutions.automation.yaf.core.processor.finish.test.log.driver.DriverLogProcessor;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class DriverResolver {

    @Autowired(required = false)
    DriverLogProcessor loggingManager;

    public boolean canResolve(Device device) {
        return device.getType().equals(getResolverType());
    }

    public abstract DriverHolder initDriver(Device device);

    public abstract DeviceType getResolverType();

    protected boolean emptyFarm(Device device) {
        return device.getFarm() == null;
    }

    protected boolean farmName(Device device, String name) {
        return !emptyFarm(device) && device.getFarm().getName().equalsIgnoreCase(name);
    }

    protected DriverHolder buildHolder(Device device, WebDriver webDriver, DriverProperties properties) {
        DriverHolder driverHolder = new DriverHolder(device, properties);
        driverHolder.setDriver(webDriver);
        return driverHolder;
    }

    // TODO validate that logging works
    protected Capabilities buildCapabilitiesFromEnv(Device device) {
        DesiredCapabilities capabilities = new DesiredCapabilities(device.getCapabilities());
        if (loggingManager != null) {
            LoggingPreferences loggingPreferences = loggingManager.getLoggingConfig();
            if (loggingPreferences != null) {
                capabilities.setCapability("goog:loggingPrefs", loggingPreferences);
            }
        }
        return capabilities;
    }

}
