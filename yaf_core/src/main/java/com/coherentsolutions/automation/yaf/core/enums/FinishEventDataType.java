
package com.coherentsolutions.automation.yaf.core.enums;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

public enum FinishEventDataType implements YafEnum {
    DRIVER_LOG("driverLog", "text/plain", "log"), SCREENSHOT("screenshot", "image/png", "png"),
    API_LOG("apiLog", "application/json", "json"), PARAM("param", "text/plain", "txt"),
    PAGE_SOURCE("pageSource", "text/html", "html");

    @JsonValue
    private final String name;
    @Getter
    private final String dataType;
    @Getter
    private final String fileExt;

    FinishEventDataType(String name, String dataType, String fileExt) {
        this.name = name;
        this.dataType = dataType;
        this.fileExt = fileExt;
    }

    public static FinishEventDataType valueOfName(String name) {
        for (FinishEventDataType e : values()) {
            if (e.name.equals(name)) {
                return e;
            }
        }
        return null;
    }

}
