
package com.coherentsolutions.automation.yaf.core.events.test;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.events.Event;
import com.coherentsolutions.automation.yaf.core.processor.finish.test.TestLogData;
import com.coherentsolutions.automation.yaf.core.test.model.TestInfo;
import com.coherentsolutions.automation.yaf.core.test.model.TestResult;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ToString(callSuper = true)
public class TestFinishEvent extends Event {

    TestInfo testInfo;
    TestResult testResult;
    Map<String, Object> data = new HashMap<>();

    List<TestLogData> logData = new ArrayList<>();

    // public void addData(FinishEventDataType dataType, String key, Object value) {
    // addData(dataType.name(), key, value);
    // }
    //
    // public void addData(String dataType, String key, Object value) {
    // Map<String, Object> objectMap = data.get(dataType);
    // if (objectMap == null) {
    // objectMap = new HashMap<>();
    // }
    // objectMap.put(key, value);
    // data.put(dataType, objectMap);
    // }
    //
    // public void addData(FinishEventDataType dataType, Map dataMap) {
    // addData(dataType.name(), dataMap);
    // }
    //
    // public void addData(String dataType, Map dataMap) {
    // Map<String, Object> objectMap = data.get(dataType);
    // if (objectMap == null) {
    // objectMap = new HashMap<>();
    // }
    // objectMap.putAll(dataMap);
    // data.put(dataType, objectMap);
    // }
    //
    // public Map<String, Object> getData(FinishEventDataType dataType) {
    // return data.get(dataType.name());
    // }

    // Map<String, String> driverLogs;
    // Map<String, byte[]> screenshots;
    // Map<String, Object> params;
    // List<ApiCallLog> apiLogs;
    // String pageSource;

}
