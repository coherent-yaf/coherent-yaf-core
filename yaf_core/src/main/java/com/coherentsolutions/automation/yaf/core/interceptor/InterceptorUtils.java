package com.coherentsolutions.automation.yaf.core.interceptor;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Deprecated
public class InterceptorUtils {

    public static Object interceptMethod(List<MethodCallInterceptor> mciList, Object instance, Method method,
            Object[] args) throws Exception {
        Map<MethodCallInterceptor, InterceptorContext> mciContext = new HashMap<>();
        for (MethodCallInterceptor mci : mciList) {
            if (mci.isApplicableToMethod(method)) {
                InterceptorContext ctx = mci.onStart(instance, method, args);
                mciContext.put(mci, ctx);
            }
        }
        Object result = null;
        Throwable ex = null;
        try {
            result = method.invoke(instance, args);
            return result;
        } catch (Throwable e) {
            ex = e;
            throw e;
        } finally {
            for (MethodCallInterceptor mci : mciList) {
                if (mciContext.containsKey(mci)) {
                    InterceptorContext ctx = mciContext.get(mci);
                    mci.onFinish(instance, method, args, result, ex, ctx);
                }
            }
        }
    }

}
