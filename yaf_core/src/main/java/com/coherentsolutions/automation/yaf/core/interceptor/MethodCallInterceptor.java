package com.coherentsolutions.automation.yaf.core.interceptor;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.utils.YafBeanUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Deprecated
@Component
public abstract class MethodCallInterceptor {

    @Autowired
    protected YafBeanUtils beanUtils;

    protected TestExecutionContext getTestExecutionContext() {
        return beanUtils.tec();
    }

    public abstract boolean isApplicableToMethod(Method method);

    public abstract InterceptorContext onStart(Object obj, Method method, Object[] args);

    public abstract void onFinish(Object obj, Method method, Object[] args, Object result, Throwable e,
            InterceptorContext context);

    protected Object proceed(ProceedingJoinPoint joinPoint) throws Throwable {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        InterceptorContext ctx = onStart(joinPoint.getThis(), method, joinPoint.getArgs());
        Object result = null;
        Throwable ex = null;
        try {
            result = joinPoint.proceed(joinPoint.getArgs());
            return result;
        } catch (Throwable e) {
            ex = e;
            throw e;
        } finally {
            onFinish(joinPoint.getThis(), method, joinPoint.getArgs(), result, ex, ctx);
        }
    }

}
