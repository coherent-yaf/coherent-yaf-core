
package com.coherentsolutions.automation.yaf.core.listener;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.ExecutionService;
import com.coherentsolutions.automation.yaf.core.config.env.domain.Environment;
import com.coherentsolutions.automation.yaf.core.config.env.resolver.ConfigurationResolver;
import com.coherentsolutions.automation.yaf.core.context.execution.ExecutionContext;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.events.global.ExecutionStartEvent;
import com.coherentsolutions.automation.yaf.core.events.global.SuiteStartEvent;
import com.coherentsolutions.automation.yaf.core.events.test.ClassFinishEvent;
import com.coherentsolutions.automation.yaf.core.events.test.RawTestFinishEvent;
import com.coherentsolutions.automation.yaf.core.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.core.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.core.processor.finish.test.TestFinishEventProcessor;
import com.coherentsolutions.automation.yaf.core.processor.finish.test.TestLogData;
import com.coherentsolutions.automation.yaf.core.processor.finish.test.properties.TestFinishProperties;
import com.coherentsolutions.automation.yaf.core.utils.PropertiesUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class GlobalYafEventsListener extends YafListener {

    @Autowired
    PropertiesUtils propertiesUtils;

    @Autowired
    ExecutionContext executionContext;
    @Autowired(required = false)
    List<TestFinishEventProcessor> finishEventProcessorList;
    @Autowired
    TestFinishProperties properties;

    @Autowired
    List<ConfigurationResolver> configurationResolversList;

    @Autowired(required = false)
    Environment stubEnv;

    boolean inited;

    // /**
    // * This is hack method, cause spring does not send start event on early phases
    // */
    // @PostConstruct
    // protected void sendStartEvent() { // TODO double check?
    // if (!inited) {
    // Map<String, String> mapProperties = (Map) executionContext.getProperties();
    // ExecutionStartEvent executionStartEvent = new ExecutionStartEvent();
    // executionStartEvent.setStartTime(LocalDateTime.now())
    // .setEnvConfigName(ExecutionService.getInstance().getConfiguration().getName())
    // .setTestEnv(executionContext.getTestEnv()).setEnvProps(mapProperties);
    //
    // eventsService.sendEvent(executionStartEvent);
    // inited = true;
    // }
    // }

    @EventListener
    @Order(1)
    public void beforeSuite(SuiteStartEvent event) {
        if (!inited) {
            synchronized (GlobalYafEventsListener.class) {
                Map<String, String> mapProperties = (Map) executionContext.getProperties();
                ExecutionStartEvent executionStartEvent = new ExecutionStartEvent();
                executionStartEvent.setStartTime(LocalDateTime.now())
                        .setEnvConfigName(ExecutionService.getInstance().getConfiguration().getName())
                        .setTestEnv(executionContext.getTestEnv()).setEnvProps(mapProperties);

                eventsService.sendEvent(executionStartEvent);
                inited = true;
            }
        }
    }

    @EventListener
    @Order(1)
    public void beforeTest(TestStartEvent event) {

        TestExecutionContext testExecutionContext = getTestExecutionContext();
        Environment env;
        if (stubEnv == null) {
            String envConf = event.getTestInfo().getEnvSetup();
            env = ExecutionService.getInstance().getEnv(envConf, testExecutionContext);
        } else {
            log.warn("Using stub env bean!");
            env = stubEnv;
        }
        // process env configurations
        if (env.getConfigs() != null) {
            env.getConfigs().entrySet().forEach(e -> {
                ConfigurationResolver configurationResolver = configurationResolversList.stream()
                        .filter(c -> c.getType().equals(e.getKey())).findFirst().orElse(null);
                if (configurationResolver != null) {
                    configurationResolver.applyConfiguration(e.getValue(), testExecutionContext);
                } else {
                    log.error("Unable to find resolver for type {} with value {}", e.getKey(), e.getValue());
                }
            });
        }

        testExecutionContext.buildFromTestStartEvent(event, env);

        event.getTestClassInstance().initTestFields();
    }

    @EventListener(RawTestFinishEvent.class)
    @Order(1)
    public void afterTest(RawTestFinishEvent event) {
        // gets raw test finish event, we need to append additional data
        log.info("After test {}", event.getTestInfo().getFullTestName());
        TestFinishEvent testFinishEvent = new TestFinishEvent();
        testFinishEvent.setTestInfo(event.getTestInfo());
        testFinishEvent.setTestResult(event.getTestResult());

        TestExecutionContext testExecutionContext = getTestExecutionContext();
        if ((properties.isOnlyFail() && !event.getTestResult().isSuccess()) || !properties.isOnlyFail()) {

            if (finishEventProcessorList != null) {
                finishEventProcessorList.forEach(p -> {
                    List<TestLogData> logData = p.processFinishEvent(testFinishEvent, testExecutionContext);
                    if (logData != null) {
                        testFinishEvent.getLogData().addAll(logData);
                    }
                });
            }
        }

        testFinishEvent.setData(testExecutionContext.getParams());
        eventsService.sendEvent(testFinishEvent);
    }

    @EventListener
    @Order(1)
    public void afterClass(ClassFinishEvent event) {
        TestExecutionContext testExecutionContext = getTestExecutionContext();
        testExecutionContext.clearContext();
    }

}
