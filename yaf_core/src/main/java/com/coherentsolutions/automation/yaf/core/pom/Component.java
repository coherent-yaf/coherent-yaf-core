
package com.coherentsolutions.automation.yaf.core.pom;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.bean.YafBean;
import com.coherentsolutions.automation.yaf.core.context.IContextual;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import com.coherentsolutions.automation.yaf.core.pom.by.SelectorType;
import com.coherentsolutions.automation.yaf.core.utils.YafBeanUtils;
import com.coherentsolutions.automation.yaf.core.utils.by.YafBy;
import com.coherentsolutions.automation.yaf.core.utils.by.YafDeviceBy;
import com.coherentsolutions.automation.yaf.core.wait.driver.DriverWaitService;
import com.coherentsolutions.automation.yaf.core.wait.driver.WaitClassUtils;
import com.coherentsolutions.automation.yaf.core.wait.driver.WaitFor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@YafComponent
@Slf4j
public abstract class Component implements IContextual, YafBean {

    @Getter
    protected TestExecutionContext testExecutionContext;

    @Getter
    protected DriverHolder driverHolder;

    @Autowired
    protected DriverWaitService waitService;
    @Autowired
    protected YafBeanUtils beanUtils;
    @Autowired
    private WaitClassUtils waitClassUtils;

    @Override
    public void setTestExecutionContext(TestExecutionContext testExecutionContext, Field field, Object obj,
            Class objectType, List<Annotation> annotations) {
        this.testExecutionContext = testExecutionContext;
        String driverName = null;
        Driver d = (Driver) annotations.stream().filter(a -> a instanceof Driver).findFirst().orElse(null);
        if (d == null) {
            d = beanUtils.getAnnotation(objectType, Driver.class);
        }
        if (d != null) {
            driverName = d.value();
        }
        init(field, obj, objectType, annotations, driverName);
    }

    public boolean isLoaded() {
        try {
            waitForLoad();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public void waitForLoad() throws WebDriverException {
        Map<Object, WaitFor> waitForFieldsForLoad = waitClassUtils.getWaitForFields(this, (w) -> w.isForLoad());
        if (!waitForFieldsForLoad.isEmpty()) {
            waitForFieldsForLoad.keySet().forEach(e -> {
                WebElement element = null;
                if (e instanceof List) {
                    List<WebElement> elements = (List<WebElement>) e;
                    // todo check
                    // seems that we already should wait all elements and calling one item will be enough
                    if (!elements.isEmpty()) {
                        element = elements.get(0);
                    }
                } else {
                    element = (WebElement) e;
                }
                if (element != null) {
                    element.getTagName();// just trigger proxy
                } else {
                    throw new WebDriverException("xxxxxx");
                }
            });
        }
    }

    protected abstract void init(Field field, Object obj, Class fieldType, List<Annotation> annotations,
            String driverName);

    public void configureComponent() {
    }

    protected YafDeviceBy __(DeviceType deviceType, YafBy by) {
        return new YafDeviceBy(testExecutionContext, driverHolder).add(deviceType, by);
    }

    protected YafBy _$(String selector) {
        return YafBy.css(selector);
    }

    protected YafBy _x(String selector) {
        return YafBy.xpath(selector);
    }

    protected YafBy _$(SelectorType type, String selector) {
        return new YafBy(type, selector);
    }

}
