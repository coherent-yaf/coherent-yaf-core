package com.coherentsolutions.automation.yaf.core.pom.by;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.bean.field.YafFieldPostProcessor;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.utils.YafBeanUtils;
import com.coherentsolutions.automation.yaf.core.utils.YafReflectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ByYafFieldPostProcessor implements YafFieldPostProcessor {

    @Autowired
    protected YafBeanUtils beanUtils;

    @Override
    public boolean canPostProcess(Field field, Class fieldType, Object fieldValue, Object obj, Class objType,
            List<Annotation> annotations, TestExecutionContext testExecutionContext) {
        return beanUtils.isChild(fieldType, By.class, WebElement.class);
    }

    @Override
    public Object postProcessField(Field field, Class fieldType, Object fieldValue, Object obj, Class objType,
            List<Annotation> annotations, TestExecutionContext testExecutionContext) {
        // TODO URGENT VALIDATE AND CACHE !!!
        Map<String, YafBy> annotationMap = YafReflectionUtils
                .getAllAnnotationsByType(annotations, YafBy.class, YafBys.class).stream()
                .collect(Collectors.toMap(a -> ((YafBy) a).key(), a -> ((YafBy) a)));

        YafBy yafBy = annotationMap.get(field.getName());
        if (yafBy != null) {
            FindBy findBy = beanUtils.getAnnotation(field, FindBy.class);
            if (findBy != null) {
                YafReflectionUtils.changeAnnotationValue(findBy, yafBy.type().getType(), yafBy.locator());
            } else {
                if (beanUtils.isChild(fieldType, By.class)) {
                    fieldValue = YafBy.YafByBuilder.buildBy(yafBy);
                } else if (fieldType.equals(String.class)) {
                    fieldValue = yafBy.locator();
                }
            }
        }

        return fieldValue;
    }
}
