package com.coherentsolutions.automation.yaf.core.pom.by;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.openqa.selenium.By;

import java.lang.annotation.*;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(YafBys.class)
@Inherited
@Documented
public @interface YafBy {

    String key() default "elementLocator";

    String locator();

    SelectorType type() default SelectorType.CSS;

    class YafByBuilder {
        public static By buildBy(YafBy yafBy) {
            String l = yafBy.locator();
            switch (yafBy.type()) {
            case ID: {
                return By.id(l);
            }
            case NAME: {
                return By.name(l);
            }
            case CLASS_NAME: {
                return By.className(l);
            }
            case CSS: {
                return By.cssSelector(l);
            }
            case TAG_NAME: {
                return By.tagName(l);
            }
            case LINK_TEXT: {
                return By.linkText(l);
            }
            case XPATH: {
                return By.xpath(l);
            }
            default:
                throw new IllegalStateException("Unexpected value: " + yafBy.type());
            }
        }
    }
}
