package com.coherentsolutions.automation.yaf.core.processor.finish.test.log.driver;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.processor.finish.test.TestLogData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import static com.coherentsolutions.automation.yaf.core.consts.Consts.TEXT_TYPE;
import static com.coherentsolutions.automation.yaf.core.consts.Consts.TXT;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class DriverLog extends TestLogData {

    String logType;
    String log;

    @Override
    public String getLogDataName() {
        return logType;
    }

    @Override
    public String getContentType() {
        return TEXT_TYPE;
    }

    @Override
    public String getFileExt() {
        return TXT;
    }

    @Override
    public byte[] getData() throws Exception {
        return log.getBytes(charset);
    }
}
