package com.coherentsolutions.automation.yaf.core.report;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.events.EventsService;
import com.coherentsolutions.automation.yaf.core.events.global.ExecutionFinishEvent;
import com.coherentsolutions.automation.yaf.core.events.global.ExecutionReportReadyEvent;
import com.coherentsolutions.automation.yaf.core.events.global.ExecutionStartEvent;
import com.coherentsolutions.automation.yaf.core.events.global.SuiteStartEvent;
import com.coherentsolutions.automation.yaf.core.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.core.report.domain.ExecutionReport;
import com.coherentsolutions.automation.yaf.core.report.domain.SuiteReport;
import com.coherentsolutions.automation.yaf.core.report.domain.TestReport;
import com.coherentsolutions.automation.yaf.core.test.model.TestInfo;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Data
@Accessors(chain = true)
@Slf4j
@Service
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".report.enabled", havingValue = "true")
public class ReportService {

    ExecutionReport executionReport;

    @Autowired
    EventsService eventsService;

    @Autowired
    PrintReportService printReportService;

    public ReportService() {
        executionReport = new ExecutionReport();
    }

    @EventListener
    public void executionStartEvent(ExecutionStartEvent executionStartEvent) {
        executionReport.setStartTime(executionStartEvent.getStartTime()).setProfile(executionStartEvent.getProfile())
                .setEnvConfigName(executionStartEvent.getEnvConfigName()).setTestEnv(executionStartEvent.getTestEnv())
                .setEnvProps(executionStartEvent.getEnvProps());
    }

    @EventListener
    public void executionFinishEvent(ExecutionFinishEvent executionFinishEvent) {
        executionReport.setEndTime(executionFinishEvent.getEndTime());
        printReportService.printReport(executionReport);
        eventsService.sendEvent(new ExecutionReportReadyEvent().setReport(executionReport));
    }

    @EventListener
    public void suiteStartEvent(SuiteStartEvent suiteStartEvent) {
        SuiteReport suiteReport = new SuiteReport();
        suiteReport.setSuiteId(suiteStartEvent.getSuiteInfo().getSuiteId())
                .setName(suiteStartEvent.getSuiteInfo().getSuiteName())
                .setSuiteParams(suiteStartEvent.getSuiteInfo().getSuiteParams());
        executionReport.getSuiteReports().put(suiteReport.getSuiteId(), suiteReport);

    }

    @EventListener
    public void testFinishEvent(TestFinishEvent testFinishEvent) {
        TestInfo testInfo = testFinishEvent.getTestInfo();
        String suiteId = testInfo.getSuiteInfo().getSuiteId();

        TestReport testReport = new TestReport(testFinishEvent.getTestResult());
        BeanUtils.copyProperties(testInfo, testReport);
        BeanUtils.copyProperties(testFinishEvent, testReport);

        SuiteReport suiteReport = executionReport.getSuiteReports().get(suiteId);
        if (suiteReport != null) {
            suiteReport.getTestReports().add(testReport);
        } else {
            log.error("Unable to add test result to report, cause suite can not be found!");
        }
    }
}
