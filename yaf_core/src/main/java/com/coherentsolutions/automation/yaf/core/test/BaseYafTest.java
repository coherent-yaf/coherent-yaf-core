
package com.coherentsolutions.automation.yaf.core.test;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.bean.factory.YafBeanProcessor;
import com.coherentsolutions.automation.yaf.core.bean.factory.YafContextFieldPostProcessor;
import com.coherentsolutions.automation.yaf.core.context.IContextual;
import com.coherentsolutions.automation.yaf.core.context.execution.ExecutionContext;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.core.events.EventsService;
import com.coherentsolutions.automation.yaf.core.exception.BeanInitYafException;
import com.coherentsolutions.automation.yaf.core.pom.Page;
import com.coherentsolutions.automation.yaf.core.utils.YafBeanUtils;
import com.github.javafaker.Faker;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Slf4j
public abstract class BaseYafTest implements IContextual {

    @Autowired
    @Getter
    protected ApplicationContext applicationContext;

    @Autowired
    @Getter
    protected ExecutionContext executionContext;

    @Autowired
    @Getter
    @Setter
    protected TestExecutionContext testExecutionContext;

    @Autowired
    protected EventsService eventsService;
    @Autowired
    protected Faker faker;
    @Autowired
    YafContextFieldPostProcessor contextFieldPostProcessor;
    @Autowired
    YafBeanUtils beanUtils;
    @Autowired
    private YafBeanProcessor beanProcessor;

    // @PostConstruct
    public void initTestFields() {
        beanProcessor.processBean(this, beanUtils.getAnnotations(this.getClass()), testExecutionContext);
    }

    protected <T extends Page> T getPage(Class pageClass) throws BeanInitYafException {
        return getObject(pageClass);
    }

    protected <T> T getObject(Class pageClass, List<Annotation>... annotations) throws BeanInitYafException {
        return (T) contextFieldPostProcessor.processField(null, pageClass, null, null,
                annotations.length > 0 ? annotations[0] : Collections.emptyList(), testExecutionContext);
    }

    protected DriverHolder getWebDriver(String... name) {
        return testExecutionContext.getWebDriverHolder(name);
    }

    protected DriverHolder getMobileDriver(String... name) {
        return testExecutionContext.getMobileDriverHolder(name);
    }

    protected DriverHolder getDesktopDriver(String... name) {
        return testExecutionContext.getDesktopDriverHolder(name);
    }

    @Override
    public void setTestExecutionContext(TestExecutionContext testExecutionContext, Field field, Object obj,
            Class objType, List<Annotation> annotations) {

        log.info("Setting TEC for {} [{}]", this.getClass().getName(), Thread.currentThread().getName());
        this.testExecutionContext = testExecutionContext;
    }
}
