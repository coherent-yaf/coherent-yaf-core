
package com.coherentsolutions.automation.yaf.core.test.model;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.test.YafTest;
import com.coherentsolutions.automation.yaf.core.utils.YafReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.logging.log4j.util.Strings;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Accessors(chain = true)
public class TestInfo {

    @Getter
    String testId;
    @Getter
    String testName;
    @Getter
    String testMethodName;

    @Getter
    @Setter
    SuiteInfo suiteInfo;

    @Getter
    String fullTestName;
    @Getter
    Method testMethod;
    @Getter
    Class testClass;
    @Getter
    @Setter
    Map<String, Object> testMethodParams;
    @Getter
    @Setter
    Map<String, String> testParams;

    @Getter
    List<Annotation> annotations;

    @Getter
    YafTest yafTest;
    @Getter
    @Setter
    String envSetup;
    @Getter
    @Setter
    Object runnerContext;

    public TestInfo(Method method, Object[] methodArgs, Function<List<Annotation>, String>... vendorTestNameResolver) {
        testId = UUID.randomUUID().toString();
        testMethod = method;
        testMethodName = method.getName();
        testClass = method.getDeclaringClass();
        annotations = YafReflectionUtils.getAnnotations(method);

        yafTest = YafReflectionUtils.findAnnotation(annotations, YafTest.class);
        if (yafTest != null && !Strings.isEmpty(yafTest.name())) {
            testName = yafTest.name();
        } else {
            if (vendorTestNameResolver.length > 0) {
                for (Function<List<Annotation>, String> resolver : vendorTestNameResolver) {
                    testName = resolver.apply(annotations);
                    if (testName != null) {
                        break;
                    }
                }
            }
            if (Strings.isEmpty(testName)) {
                testName = method.getName();
            }
        }
        if (methodArgs != null && methodArgs.length > 0) {
            testMethodParams = new HashMap<>();
            List<String> methodParamsNames = Arrays.stream(method.getParameters()).map(p -> p.getName())
                    .collect(Collectors.toList());
            for (int i = 0; i < methodParamsNames.size(); i++) {
                String paramName = methodParamsNames.get(i);
                Object paramValue = methodArgs[i];
                testMethodParams.put(paramName, paramValue);
            }
        }
    }

    public String getFullTestName() {
        if (fullTestName == null) {
            fullTestName = testClass.getName() + "." + testMethodName + "[" + testName + "]";
        }
        return fullTestName;
    }

}
