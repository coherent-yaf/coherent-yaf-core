package com.coherentsolutions.automation.yaf.core.utils;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Service
public class DateUtils {

    public static String getCurrentDateTimeByPatternAndZone(String pattern, String zone) {
        ZonedDateTime now = isNotEmpty(zone) ? ZonedDateTime.now(ZoneId.of(zone)) : ZonedDateTime.now();
        return now.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static String getCurrentDateTimeByPattern(String pattern) {
        return getCurrentDateTimeByPatternAndZone(pattern, null);
    }

    public static Date getDateFromStringByPattern(String date, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static int getMonthValueFromStringByPattern(String date, String pattern) {
        return getDateFromStringByPattern(date, pattern).toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                .getMonthValue();
    }
}
