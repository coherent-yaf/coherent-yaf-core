
package com.coherentsolutions.automation.yaf.core.utils;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.exception.GeneralYafException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

public class FileUtils {

    protected static ObjectMapper mapper;

    public static String normalizeFileName(String name, String ext) {
        if (!name.endsWith(ext)) {
            if (!ext.startsWith(".")) {
                ext = "." + ext;
            }
            name += ext;
        }
        return name;
    }

    public static File getResourceJsonFile(String name, String resourceFolder) throws GeneralYafException {
        return getResourceFile(name, Consts.JSON, resourceFolder, true, false);
    }

    public static File getResourceJson5File(String name, String resourceFolder) throws GeneralYafException {
        try {
            return getResourceFile(name, Consts.JSON5, resourceFolder, true, false);
        } catch (GeneralYafException e) {
            return getResourceJsonFile(name, resourceFolder);
        }
    }

    public static File getResourceFile(String name, String ext, String resourceFolder, boolean normalize,
            boolean tryToLoadExternal) throws GeneralYafException {
        if (normalize) {
            name = FileUtils.normalizeFileName(name, ext);
        }
        String classpathPath = ResourceUtils.CLASSPATH_URL_PREFIX + (resourceFolder != null ? resourceFolder + "/" : "")
                + name;
        try {
            File f = ResourceUtils.getFile(classpathPath);
            if (f.exists()) {
                return f;
            }
        } catch (FileNotFoundException e) {
            // no need
        }
        if (tryToLoadExternal) {
            try {
                File f = ResourceUtils.getFile("." + File.separator + name);
                if (f.exists()) {
                    return f;
                }
            } catch (FileNotFoundException fileNotFoundException) {
                // no need
            }
        }
        throw new GeneralYafException("File " + name + " not found!");
    }

    public static ObjectMapper getObjectMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.enable(JsonParser.Feature.ALLOW_COMMENTS);
            mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        }
        return mapper;
    }

}
