package com.coherentsolutions.automation.yaf.core.utils;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.springframework.core.annotation.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.lang.annotation.Repeatable;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class YafReflectionUtils {

    public static Object changeAnnotationValue(Annotation annotation, String key, Object newValue) {
        Object handler = Proxy.getInvocationHandler(annotation);
        Field f;
        try {
            f = handler.getClass().getDeclaredField("memberValues");
        } catch (NoSuchFieldException | SecurityException e) {
            throw new IllegalStateException(e);
        }
        f.setAccessible(true);
        Map<String, Object> memberValues;
        try {
            memberValues = (Map<String, Object>) f.get(handler);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        Object oldValue = memberValues.get(key);
        if (oldValue == null || oldValue.getClass() != newValue.getClass()) {
            throw new IllegalArgumentException();
        }
        memberValues.put(key, newValue);
        return oldValue;
    }

    public static void setFieldValue(String fieldName, Object fieldValue, Object obj) {
        try {
            Field field = obj.getClass().getField(fieldName);
            setFieldValue(field, fieldValue, obj);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void setFieldValue(Field field, Object fieldValue, Object obj) {
        try {
            field.setAccessible(true);
            field.set(obj, fieldValue);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static List<Annotation> getAnnotations(AnnotatedElement e) {
        return Arrays.stream(e.getDeclaredAnnotations()).collect(Collectors.toList());
    }

    public static <T extends Annotation> T getAnnotation(AnnotatedElement e, Class annotationCls) {
        return (T) AnnotationUtils.getAnnotation(e, annotationCls);
    }

    public static <T extends Annotation> T findAnnotation(List<Annotation> annotations, Class cls) {
        return (T) annotations.stream().filter(a -> a.annotationType().equals(cls)).findFirst().orElse(null);
    }

    public static boolean isChild(Class child, List<Class> parents) {
        return parents.stream().filter(p -> p.isAssignableFrom(child)).findAny().isPresent();
    }

    public static boolean isChild(Class child, Class... parents) {
        if (parents.length > 0) {
            return isChild(child, Arrays.asList(parents));
        } else {
            return false;
        }
    }

    public static <T extends Annotation> List<T> getAllAnnotationsByType(AnnotatedElement e, Class baseAnnotation,
            Class repeaterAnnotation) {
        return getAllAnnotationsByType(getAnnotations(e), baseAnnotation, repeaterAnnotation);
    }

    public static <T extends Annotation> List<T> getAllAnnotationsByType(AnnotatedElement e, Class baseAnnotation) {
        return getAllAnnotationsByType(getAnnotations(e), baseAnnotation);
    }

    public static <T extends Annotation> List<T> getAllAnnotationsByType(List<Annotation> annotations,
            Class baseAnnotation) {
        Repeatable repeatable = AnnotationUtils.getAnnotation(baseAnnotation, Repeatable.class);
        if (repeatable != null) {
            return getAllAnnotationsByType(annotations, baseAnnotation, repeatable.value());
        }
        return getAllAnnotationsByType(annotations, baseAnnotation, null);
    }

    public static <T extends Annotation> List<T> getAllAnnotationsByType(List<Annotation> annotations,
            Class baseAnnotation, Class repeaterAnnotation) {
        List<Annotation> annotationList = annotations.stream().filter(a -> a.annotationType().equals(baseAnnotation))
                .collect(Collectors.toList());
        if (repeaterAnnotation != null) {
            Annotation repeatedAnnotation = annotations.stream()
                    .filter(a -> a.annotationType().equals(repeaterAnnotation)).findFirst().orElse(null);
            if (repeatedAnnotation != null) {
                Annotation[] rValue = (Annotation[]) AnnotationUtils.getAnnotationAttributes(repeatedAnnotation)
                        .get("value");
                annotationList.addAll(Arrays.asList(rValue));
            }
        }
        return (List<T>) annotationList;
    }

}
