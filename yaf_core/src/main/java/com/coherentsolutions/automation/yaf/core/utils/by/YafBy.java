
package com.coherentsolutions.automation.yaf.core.utils.by;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.pom.by.SelectorType;
import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import java.util.List;

@Data
public class YafBy extends By {

    SelectorType type;
    String selector;
    By by;

    public YafBy(By by) {
        this.by = by;
    }

    public YafBy(SelectorType type, String selector) {
        this.type = type;
        this.selector = selector;
    }

    public static YafBy css(String selector) {
        return new YafBy(SelectorType.CSS, selector);
    }

    public static YafBy xpath(String selector) {
        return new YafBy(SelectorType.XPATH, selector);
    }

    public static YafBy by(By by) {
        return new YafBy(by);
    }

    public YafBy addSelector(String extraSelector) {
        return new YafBy(type, selector + (type.equals(SelectorType.CSS) ? " " : "") + extraSelector);
    }

    protected By getBy() {
        if (by != null) {
            return by;
        }
        switch (type) {
        case ID: {
            return By.id(selector);
        }
        case NAME: {
            return By.name(selector);
        }
        case CLASS_NAME: {
            return By.className(selector);
        }
        case CSS: {
            return By.cssSelector(selector);
        }
        case TAG_NAME: {
            return By.tagName(selector);
        }
        case LINK_TEXT: {
            return By.linkText(selector);
        }
        case XPATH: {
            return By.xpath(selector);
        }
        default:
            throw new IllegalStateException("Unexpected value: " + type);
        }
    }

    @Override
    public List<WebElement> findElements(SearchContext context) {
        return context.findElements(getBy());
    }

    @Override
    public WebElement findElement(SearchContext context) {
        return context.findElement(getBy());
    }

}
