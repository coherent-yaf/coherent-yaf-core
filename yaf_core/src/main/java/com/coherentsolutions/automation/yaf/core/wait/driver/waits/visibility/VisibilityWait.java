
package com.coherentsolutions.automation.yaf.core.wait.driver.waits.visibility;

/*-
 * #%L
 * Yaf Core
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.wait.driver.BaseWait;
import com.coherentsolutions.automation.yaf.core.wait.driver.WaitFor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.annotation.Annotation;
import java.util.List;

public class VisibilityWait extends BaseWait {

    boolean invisible;

    public VisibilityWait(WebElement element) {
        super(element);
    }

    public VisibilityWait(By by) {
        super(by);
    }

    public VisibilityWait(WaitFor waitFor, Annotation waitAnnotation, List<WebElement> elements) {
        super(waitFor, waitAnnotation, elements);
    }

    public VisibilityWait withInvisible(boolean invisible) {
        this.invisible = invisible;
        return this;
    }

    protected void processCustomWaitAnnotation() {
        try {
            WaitForVisible waitForVisible = (WaitForVisible) waitAnnotation;
            invisible = waitForVisible.invisible();
        } catch (ClassCastException ex) {
            // to support default behaviour for waitFor annotation
        }
    }

    @Override
    protected <R> R wait(WebDriverWait wait) {
        if (invisible) {
            return (R) wait.until(ExpectedConditions.invisibilityOfAllElements(getElements()));
        } else {
            return (R) wait.until(ExpectedConditions.visibilityOfAllElements(getElements()));
        }
    }
}
