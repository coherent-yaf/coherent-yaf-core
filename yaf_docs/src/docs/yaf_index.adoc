= YAF Users Guid

:sectnums:
:Author:    Artem Moroz
:Email:     <artemmoroz@coherentsolutions.com>
:Date:      18/02/2021
:Revision:  0.1

# Source codes

YAF Core: {sourceCode}coherent-yaf-core/src/master/

YAF Modules: {sourceCode}coherent-yaf-modules/src/master/

include::overview.adoc[]
include::firstTest.adoc[]

== YAF Core features

include::core_index.adoc[]

== Extras

// modules include

=== TestNg

include::testng.adoc[]
include::dataprovider.adoc[]

=== RestAssured

include::api_restassured_en.adoc[]

=== Allure

include::allure.adoc[]
include::allureTestng.adoc[]

=== Selenide

include::selenide.adoc[]

include::contributors.adoc[]
include::releases.adoc[]
include::faq.adoc[]


