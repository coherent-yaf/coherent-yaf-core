package com.coherentsolutions.automation.yaf.docs;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class DirectoryFlatter {

    public static void main(String[] args) throws IOException {
        if (args.length > 0) {
            Path root = Paths.get(args[0]);
            Path imagesPath = root.resolve("images");
            File images = imagesPath.toFile();
            if (images.exists()) {
                FileUtils.cleanDirectory(images);
            } else {
                FileUtils.forceMkdir(images);
            }

            List<File> dirsToDelete = new ArrayList<>();
            Files.walk(root).forEach(path -> {
                File f = path.toFile();
                String name = f.getName();
                System.out.println("Start processing " + name);
                if (f.isDirectory()) {
                    if (!name.equals("images") && !name.equals("docs")) {
                        dirsToDelete.add(f);
                    }
                } else {
                    try {
                        if (name.endsWith(".adoc")) {
                            //copy to docs folder
                            System.out.println("Will copy " + f.toPath());
                            Files.copy(f.toPath(), root.resolve(name), StandardCopyOption.REPLACE_EXISTING);
                        } else {
                            //copy to images
                            Files.copy(f.toPath(), imagesPath.resolve(name), StandardCopyOption.REPLACE_EXISTING);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            dirsToDelete.forEach(dir -> FileUtils.deleteQuietly(dir));
            System.out.println("Processing complete. " + args[0]);
        } else {
            System.out.println("Unable to process docs, root directory not specified!");
            System.exit(1);
        }
    }
}
