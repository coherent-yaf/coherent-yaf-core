package com.coherentsolutions.automation.yaf.docs;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class DocsPostProcessor {

    public static void main(String[] args) throws IOException {
        if (args.length > 0) {
            Path root = Paths.get(args[0]);
            Files.walk(root).forEach(path -> {

                File f = path.toFile();
                String name = f.getName();

                if (name.endsWith(".html")) {
                    try {
                        List<String> lines = Files.readAllLines(path).stream().map(l -> l.replaceAll("/images/images/", "/images/")).collect(Collectors.toList());
                        Files.write(path, lines);
                    } catch (IOException e) {
                        //
                    }
                }

            });
        } else {
            System.out.println("Unable to process docs, root directory not specified!");
            System.exit(1);
        }
    }
}
